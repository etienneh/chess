package chess;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HorseTest {

    @Test
    void shouldReturnPossiblePositions() {
        ChessField chessField = new ChessField();
        chessField.setSizeX(8);
        chessField.setSizeY(8);
        ChessFigure whiteHorse = new Horse();

        List<PossiblePosition> possiblePositions = whiteHorse.getPossiblePositionsWithOpponentPositions(chessField);

        //assertions are only implemented for point x=8 y=8
        PossiblePosition positionX1Y1 = possiblePositions.get(possiblePositions.size() - 1);
        Assertions.assertEquals(new Point(8, 8), positionX1Y1.getPosition());
        List<Point> opponentPositions = positionX1Y1.getOpponentPositions();
        Assertions.assertTrue(opponentPositions.contains(new Point(7, 6)));
        Assertions.assertTrue(opponentPositions.contains(new Point(6, 7)));
        Assertions.assertEquals(2, opponentPositions.size());
    }

}
