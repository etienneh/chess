package chess;

import java.util.List;

public class ChessGame {

    public static void main(String[] args) {
        ChessField chessField = new ChessField();
        chessField.setSizeX(8);
        chessField.setSizeY(8);
        ChessFigure whiteHorse = new Horse();

        List<PossiblePosition> possiblePositions = whiteHorse.getPossiblePositionsWithOpponentPositions(chessField);

        for (PossiblePosition possiblePosition : possiblePositions) {
            System.out.println("white position: " + possiblePosition.getPosition() + " possible black positions: " + possiblePosition.getOpponentPositions());
        }
    }

}
