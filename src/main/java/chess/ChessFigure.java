package chess;

import java.util.List;

public interface ChessFigure {

    List<PossiblePosition> getPossiblePositionsWithOpponentPositions(ChessField chessField);

}
