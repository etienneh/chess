package chess;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Horse implements ChessFigure {

    private static final int STRAIGHT_MOVE = 2;
    private static final int LATERAL_MOVE = 1;

    public List<PossiblePosition> getPossiblePositionsWithOpponentPositions(ChessField chessField) {
        List<PossiblePosition> possiblePositions = new ArrayList<>();
        for (int x = 1; x <= chessField.getSizeX(); x++) {
            for (int y = 1; y <= chessField.getSizeY(); y++) {
                boolean          isExceededUpRight   = y + STRAIGHT_MOVE > chessField.getSizeY() || x + LATERAL_MOVE > chessField.getSizeX();
                boolean          isExceededUpLeft    = y + STRAIGHT_MOVE > chessField.getSizeY() || x - LATERAL_MOVE < 1;
                boolean          isExceededRightUp   = x + STRAIGHT_MOVE > chessField.getSizeX() || y + LATERAL_MOVE > chessField.getSizeY();
                boolean          isExceededRightDown = x + STRAIGHT_MOVE > chessField.getSizeX() || y - LATERAL_MOVE < 1;
                boolean          isExceededDownRight = y - STRAIGHT_MOVE < 1 || x + LATERAL_MOVE > chessField.getSizeX();
                boolean          isExceededDownLeft  = y - STRAIGHT_MOVE < 1 || x - LATERAL_MOVE < 1;
                boolean          isExceededLeftUp    = x - STRAIGHT_MOVE < 1 || y + LATERAL_MOVE > chessField.getSizeY();
                boolean          isExceededLeftDown  = x - STRAIGHT_MOVE < 1 || y - LATERAL_MOVE < 1;

                PossiblePosition possiblePosition    = new PossiblePosition();
                possiblePosition.setPosition(new Point(x, y));
                List<Point> blackPositions = new ArrayList<>();
                if (!isExceededUpRight) {
                    blackPositions.add(new Point(x + LATERAL_MOVE, y + STRAIGHT_MOVE));
                }
                if (!isExceededUpLeft) {
                    blackPositions.add(new Point(x - LATERAL_MOVE, y + STRAIGHT_MOVE));
                }
                if (!isExceededRightUp) {
                    blackPositions.add(new Point(x + STRAIGHT_MOVE, y + LATERAL_MOVE));
                }
                if (!isExceededRightDown) {
                    blackPositions.add(new Point(x + STRAIGHT_MOVE, y - LATERAL_MOVE));
                }
                if (!isExceededDownRight) {
                    blackPositions.add(new Point(x + LATERAL_MOVE, y - STRAIGHT_MOVE));
                }
                if (!isExceededDownLeft) {
                    blackPositions.add(new Point(x - LATERAL_MOVE, y - STRAIGHT_MOVE));
                }
                if (!isExceededLeftUp) {
                    blackPositions.add(new Point(x - STRAIGHT_MOVE, y + LATERAL_MOVE));
                }
                if (!isExceededLeftDown) {
                    blackPositions.add(new Point(x - STRAIGHT_MOVE, y - LATERAL_MOVE));
                }
                possiblePosition.setOpponentPositions(blackPositions);
                possiblePositions.add(possiblePosition);
            }
        }
        return possiblePositions;
    }

}
