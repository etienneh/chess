package chess;

import lombok.Data;

@Data
public class ChessField {

    private int sizeX;
    private int sizeY;

}
