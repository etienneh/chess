package chess;

import lombok.Data;

import java.awt.*;
import java.util.List;

@Data
public class PossiblePosition {

    private Point       position;
    private List<Point> opponentPositions;

}
